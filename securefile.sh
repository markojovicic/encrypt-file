#!/bin/bash
#
# Decrypt file in temp file,
# open it in vim for editing,
# and after it is closed file is encrypted back into origin location.
# Previous origin encrypted file is backed up.
#
set -e

# $1 original encrypted to replace if changes occurs
# $2 decrypted file to change
function modifyFile() {
    orig="$1"
    decr="$2"
     # edit file
    lastmodif=$(date +%s -r "$decr")
    vim "$decr"
    lastmodif2=$(date +%s -r "$decr")
    
    if [ "$lastmodif" -ne "$lastmodif2" ]; then
        backup "$orig"
        encrypt "$decr" "$orig"
    fi

    # decrypted file is deleted
    rm -f "$decr"
}

# $1 to backup - just append date
function backup() {
    if [ -f "$1" ]; then
    
        # backup original file
        mv "$1" "${1}.$(($(date +%s%N)/1000000))"
    fi
}

# $1 file to encrypt
# $2 output of encrypted file
function encrypt() {
    openssl des3 < "$1" > "$2"
}

# $1 file to decrypt
# $2 output of decrypted file
function decrypt() {
    openssl des3 -d < "$1" > "$2"
}

#
# Main program starts here
#

decrypted=$(tempfile)

if [ ! -f "$1" ]; then
    # new file
    modifyFile "$1" "$decrypted" 
else 
    decrypt "$1" "$decrypted"
    modifyFile "$1" "$decrypted"
fi


